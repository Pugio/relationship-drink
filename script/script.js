// RADAR CHART:
Chart.defaults.global.legend = false;

// Variablen Radar
var sweet = document.getElementById('sweetness');
var temp = document.getElementById('temperature');
var alc = document.getElementById('alcoholic_content');
var spicy = document.getElementById('spiciness');
var brewing_time = document.getElementById('brewing_time');

// Setting initial Values of Radar
set_init_values();

// Radar Chart
var ctxR = document.getElementById("radarChart").getContext('2d');

// Drink Name Label
var drinkName = document.getElementById('drinkName');

// Glass Image Variables
var glass_back = document.getElementById('glass_back');
var glass_filling = document.getElementById('glass_filling');
var glass_front = document.getElementById('glass_front');
var steam = document.getElementById('steam');
var frost = document.getElementById('frost');

// Init Configuration of Radar
var config = {
    type: 'radar',
    data: {
      labels: ["Sweetness", "Temperature", "Alcoholic Content", "Spiciness", "Brewing Time"],
      datasets: [{
          label: "Relationship Drink",
          data: [sweet.value, temp.value, alc.value, spicy.value, brewing_time.value],
          backgroundColor: [
            'rgba(105, 0, 132, .2)',
          ],
          borderColor: [
            'rgba(200, 99, 132, .7)',
          ],
          borderWidth: 2
        }
      ]
    },
    options: {
        responsive: true,
        scale: {
            ticks: {
                beginAtZero: true
            }
        }
    }
};

// Create the Radar Chart
var radarChart = new Chart(ctxR, config);
checkDrink();
changePic();

function updateRadar(){
    console.log('sweet: ' + sweet.value + ' | temp: ' + temp.value + ' | alc: ' + alc.value + ' | spicy: ' +
    spicy.value + ' | brewing time: ' + brewing_time.value);
    config.data.datasets[0].data[0] = sweet.value;
    config.data.datasets[0].data[1] = temp.value;
    config.data.datasets[0].data[2] = alc.value;
    config.data.datasets[0].data[3] = spicy.value;
    config.data.datasets[0].data[4] = brewing_time.value;

    radarChart.update();
    
    checkDrink();
    changePic();
    changeUrl();
}

function checkDrink(){
    if(sweet.value == 100 & temp.value == 100 & alc.value == 100){
        drinkName.innerHTML = "Cola mit Mentos";
    }

    else if(sweet.value == 0 & temp.value == 0 & alc.value == 0){
        drinkName.innerHTML = "Eiswasser";
    }

    else if(sweet.value == 0 & temp.value == 0 & alc.value == 100){
        drinkName.innerHTML = "Gin Tonic";
    }

    else{
        drinkName.innerHTML = "";
    }

}

function changePic(){
    if(temp.value > 50){
        steam.style.opacity = (temp.value - 50) * 0.02;
        frost.style.opacity = 0;
    }
    else{
        steam.style.opacity = 0;
        frost.style.opacity = (50 - temp.value) * 0.02;
    }
}

function changeUrl(){
    try{
        baseURL = window.location.href.split('?')[0];
    }
    catch(error){
        baseURL = window.location.href;
    }

    fullURL = baseURL + '?sweet=' + sweet.value + 
    '&temp=' + temp.value + 
    '&alc=' + alc.value + 
    '&spicy=' + spicy.value + 
    '&brewing_time=' + brewing_time.value;

    window.location.href = fullURL;
}

// For initial Value Setting
function set_init_values() {

    // Give Default Value of 50
    sweet.value = 50;
    temp.value = 50;
    alc.value = 50;
    spicy.value = 50;
    brewing_time.value = 50;

    try{
        para = window.location.href.split('?')[1].split('&');

        for(i = 0; i < para.length ; i++){
            paraSplit = para[i].split('=');

            if(paraSplit[0] == 'sweet'){
                sweet.value = paraSplit[1];
            }
            else if(paraSplit[0] == 'temp'){
                temp.value = paraSplit[1];
            }
            else if(paraSplit[0] == 'alc'){
                alc.value = paraSplit[1];
            }
            else if(paraSplit[0] == 'spicy'){
                spicy.value = paraSplit[1];
            }
            else if(paraSplit[0] == 'brewing_time'){
                brewing_time.value = paraSplit[1];
            }
        }
    }
    catch(error){
        console.info("No Parameters in URL");
    }
}

